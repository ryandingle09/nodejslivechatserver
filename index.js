var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('room1', function(msg){
        io.emit('room1', msg);
        
        console.log('message: ' + msg);
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('live-chat', function(msg){
        io.emit('live-chat', msg);
        
        console.log('message: ' + msg);
    });

    socket.on('betting', function(data){
        io.emit('betting', data);
        
        console.log('data: ' + data);
    });

    socket.on('announce', function(data){
        io.emit('announce', data);
        
        console.log('data: ' + data);
    });
});

http.listen(9000, function(){
    console.log('listening on *:9000');
});