# build the image
sudo docker build -t chatserver .

# run the image in the container
sudo docker run -it --rm --name chatserver -d -p 9000:9000 chatserver

# run the image in the container (auto run when pc is restarted or on both)
sudo docker run --restart=always -it --name chatserver -d -p 9000:9000 chatserver


# stop containenr
sudo docker container stop chatserver 